from controlcenter import Dashboard, widgets

from azalext.retail.models import *

################################################################################

class ModelItemList(widgets.ItemList):
    model = Person
    list_display = ('pk', 'username','email')

################################################################################

class MySingleBarChart(widgets.SingleBarChart):
    # label and series
    values_list = ('username', 'score')
    # Data source
    queryset = Person.objects.order_by('-score')
    limit_to = 3

################################################################################

class Landing(Dashboard):
    title = 'Prints'

    widgets = (
        ModelItemList,
        MySingleBarChart,
    )

